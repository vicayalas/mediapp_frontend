import { Paciente } from "./paciente";
export class Signos {
    public idSignos: number;
    public fecha: Date;
    public paciente: Paciente;
    public pulso: string;
    public ritmo: string;
    public temperatura: string;
}