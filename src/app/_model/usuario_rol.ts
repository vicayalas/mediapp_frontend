import { Rol } from './rol';
import { Usuario} from './usuario';
export class UsuarioRol{
    idUsuario: Usuario;
    roles: Rol[];
}