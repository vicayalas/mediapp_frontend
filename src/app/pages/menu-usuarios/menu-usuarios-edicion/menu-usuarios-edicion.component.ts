import { Component, OnInit } from '@angular/core';
import { Rol } from '../../../_model/rol';
import { MatSnackBar } from '@angular/material';
import { RolService } from '../../../_service/rol.service';
import { Usuario } from '../../../_model/usuario';
import { UsuarioRol } from '../../../_model/usuario_rol';
import { UsuarioService } from '../../../_service/usuario.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-menu-usuarios-edicion',
  templateUrl: './menu-usuarios-edicion.component.html',
  styleUrls: ['./menu-usuarios-edicion.component.css']
})
export class MenuUsuariosEdicionComponent implements OnInit {
  roles: Rol[] = [];
  usuarios: Usuario[] = [];
  idUsuarioSeleccionado: number;
  rolesSeleccionados: Rol[] = [];
  idRolSeleccionado: number;
  mensaje: string;

  rolSeleccionado: Rol;
  rolesactuales: Rol[];

  id: number;
  usuario: Usuario;
  form: FormGroup;
  edicion: boolean = false;


  constructor(private route: ActivatedRoute, private router: Router, public snackBar: MatSnackBar, private rolService: RolService,private usuarioService: UsuarioService) {

    this.form = new FormGroup({
      'id': new FormControl(0),
      'username': new FormControl(''),
      'enabled': new FormControl(true),
      'password': new FormControl('')
    });

   }

  ngOnInit() {
   this.listarRoles();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }



  private initForm() {
    if (this.edicion) {
      this.usuarioService.listarUsuarioPorId(this.id).subscribe(data => {
        let id = data.idUsuario;
        let username = data.username;
        let password = data.password;
        let enabled = data.enabled;
        this.rolesactuales=data.roles;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'username': new FormControl(username),
          'password': new FormControl(password),
          'enabled': new FormControl(enabled)

        });
      });
    }
  }

  listarRoles() {
    this.rolService.listarRoles().subscribe(data => {
      this.roles = data;
    });
  }



  agregarRoles() {
    if (this.rolSeleccionado) {
      let cont = 0;
      for (let i = 0; i < this.rolesSeleccionados.length; i++) {
        let rol = this.rolesSeleccionados[i];
        if (rol.idRol === this.rolSeleccionado.idRol) {
          cont++;
          break;
        }
      }
      if (cont > 0) {
        this.mensaje = `El rol se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        this.rolesSeleccionados.push(this.rolSeleccionado);
      }
    } else {
      this.mensaje = `Debe agregar un rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  limpiarControles() {
    this.rolesSeleccionados = [];
   // this.diagnostico = '';
   // this.tratamiento = '';
   // this.pacienteSeleccionado = null;
    this.rolSeleccionado = null;
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }
  estadoBotonRegistrar() {
    return ( this.idUsuarioSeleccionado === 0 || this.rolesSeleccionados.length === 0);
  }

  aceptar(estado) {
    if(estado==true){
    this.usuario = new Usuario();
    
    this.usuario.idUsuario = this.form.value['id'];//this.usuarioSeleccionado;
    this.usuario.username = this.form.value['username'];
    this.usuario.password = this.form.value['password'];
    this.usuario.enabled = this.form.value['enabled'];

    this.usuario.roles = this.rolesSeleccionados;

    console.log(this.usuario);
    if (this.edicion) {
          //update
          this.usuarioService.modificar(this.usuario).subscribe(data => {
            this.usuarioService.listarUsuarios().subscribe(usuarios => {
              this.usuarioService.usuarioCambio.next(usuarios);
              this.usuarioService.mensaje.next('Se modificó');
            });
          });
        }
      
         this.router.navigate(['menu-usuarios'])

  setTimeout(() => {    this.limpiarControles();    }, 2000);
  }
  }

}
