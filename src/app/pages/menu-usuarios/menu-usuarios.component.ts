import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario } from '../../_model/usuario';
import { UsuarioService } from '../../_service/usuario.service';

import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-menu-usuarios',
  templateUrl: './menu-usuarios.component.html',
  styleUrls: ['./menu-usuarios.component.css']
})
export class MenuUsuariosComponent implements OnInit {

  lista: Usuario[] = [];
  displayedColumns = ['idUsuario', 'username','roles', 'acciones'];
  dataSource: MatTableDataSource<Usuario>;
  cantidad : number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor( public snackBar: MatSnackBar, private usuarioService: UsuarioService) { }
  ngOnInit() {
    this.usuarioService.usuarioCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.usuarioService.mensaje.subscribe(data => {        
        this.snackBar.open(data, 'Aviso', { duration: 2000 });
      });      
    });


    this.usuarioService.listarUsuariosPageable(0, 10).subscribe(data => {
     
      let usuarios = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(usuarios);
      this.dataSource.sort = this.sort;
      console.log(usuarios);
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }



  mostrarMas(e: any){
    console.log(e);
    this.usuarioService.listarUsuariosPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let usuarios = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(usuarios);
      
      this.dataSource.sort = this.sort;
      
    });
  }

}
