import { MenuService } from './../../../_service/menu.service';
import { Menu } from './../../../_model/menu';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {

  id: number;
  menu: Menu;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private menuService: MenuService, private route: ActivatedRoute, private router: Router) {
    this.menu = new Menu();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
      'url': new FormControl(''),
      'icono': new FormControl('')
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.menuService.listarMenuPorId(this.id).subscribe(data => {
        let id = data.idMenu;
        let nombre = data.nombre;
        let url = data.url;
        let icono = data.icono;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre),
          'url': new FormControl(url),
          'icono': new FormControl(icono)

        });
      });
    }
  }

  operar() {
    this.menu.idMenu = this.form.value['id'];
    this.menu.nombre = this.form.value['nombre'];
    this.menu.url = this.form.value['url'];
    this.menu.icono = this.form.value['icono'];

    if (this.menu != null && this.menu.idMenu > 0) {
      this.menuService.modificar(this.menu).subscribe(data => {
        this.menuService.listarMenus().subscribe(menu => {
          this.menuService.menuCambio.next(menu);
          this.menuService.mensaje.next("Se modificó");
        });
      });
    } else {
      this.menuService.registrar(this.menu).subscribe(data => {
        this.menuService.listarMenus().subscribe(menu => {
          this.menuService.menuCambio.next(menu);
          this.menuService.mensaje.next("Se registró");
        });
      });
    }

    this.router.navigate(['menu']);
  }
}
