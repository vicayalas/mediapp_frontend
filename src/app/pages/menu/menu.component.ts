import { MenuService } from './../../_service/menu.service';
import { Menu } from './../../_model/menu';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  displayedColumns = ['id', 'nombre', 'url', 'icono', 'acciones'];
  dataSource: MatTableDataSource<Menu>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;

  constructor(private menuService: MenuService, public route: ActivatedRoute, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.menuService.menuCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.menuService.mensaje.subscribe(data => {
      this.snackBar.open(data, null, {
        duration: 2000,
      });
    });

    this.menuService.listarMenus().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  eliminar(menu: Menu): void {
    this.menuService.eliminar(menu.idMenu).subscribe(data => {
      if (data === 1) {
        this.menuService.listarMenus().subscribe(data => {
          this.menuService.menuCambio.next(data);
          this.menuService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.menuService.mensaje.next("No se pudo eliminar");
      }
    });
  }
}
