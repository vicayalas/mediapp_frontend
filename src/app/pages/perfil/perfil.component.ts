import { Component, OnInit } from '@angular/core';
import { HOST, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, TOKEN_NAME, MICRO_AUTH } from '../../_shared/var.constant';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  username;
  roles;
  constructor() { 

let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));

  const decodedToken = decode(token.access_token);
  console.log(decodedToken);
  this.roles = decodedToken.authorities;

  this.username = decodedToken.user_name;

  }

  ngOnInit() {
    

  }

}
