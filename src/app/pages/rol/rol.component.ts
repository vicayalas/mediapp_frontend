import { RolService } from './../../_service/rol.service';
import { Rol } from './../../_model/rol';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  displayedColumns = ['id', 'nombre', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<Rol>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;

  constructor(private rolService: RolService, public route: ActivatedRoute, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.rolService.rolCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.rolService.mensaje.subscribe(data => {
      this.snackBar.open(data, null, {
        duration: 2000,
      });
    });

    this.rolService.listarRoles().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  eliminar(rol: Rol): void {
    this.rolService.eliminar(rol.idRol).subscribe(data => {
      if (data === 1) {
        this.rolService.listarRoles().subscribe(data => {
          this.rolService.rolCambio.next(data);
          this.rolService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.rolService.mensaje.next("No se pudo eliminar");
      }
    });
  }
}
