import { Component, OnInit } from '@angular/core';
import { Rol } from '../../../_model/rol';
import { MatSnackBar } from '@angular/material';
import { RolService } from '../../../_service/rol.service';
import { Menu } from '../../../_model/menu';
import { MenuService } from '../../../_service/menu.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-roles-menu-edicion',
  templateUrl: './roles-menu-edicion.component.html',
  styleUrls: ['./roles-menu-edicion.component.css']
})
export class RolesMenuEdicionComponent implements OnInit {
  roles: Rol[] = [];
  menus: Menu[] = [];
  idMenuSeleccionado: number;
  rolesSeleccionados: Rol[] = [];
  idRolSeleccionado: number;
  mensaje: string;
  rolesactuales: Rol[];

  rolSeleccionado: Rol;


  id: number;
  menu: Menu;
  form: FormGroup;
  edicion: boolean = false;


  constructor(private route: ActivatedRoute, private router: Router, public snackBar: MatSnackBar, private rolService: RolService,private menuService: MenuService) {

    this.form = new FormGroup({
      'id': new FormControl(0),
      'icono': new FormControl(''),
      'nombre': new FormControl(''),
      'url': new FormControl('')
    });

   }

  ngOnInit() {
   this.listarRoles();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }



  private initForm() {
    if (this.edicion) {
      this.menuService.listarMenuPorId(this.id).subscribe(data => {
        let id = data.idMenu;
        let icono = data.icono;
        let nombre = data.nombre;
        let url = data.url;
        this.rolesactuales=data.roles;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'icono': new FormControl(icono),
          'nombre': new FormControl(nombre),
          'url': new FormControl(url)

        });
      });
    }
  }

  listarRoles() {
    this.rolService.listarRoles().subscribe(data => {
      this.roles = data;
    });
  }



  agregarRoles() {
    if (this.rolSeleccionado) {
      let cont = 0;
      for (let i = 0; i < this.rolesSeleccionados.length; i++) {
        let rol = this.rolesSeleccionados[i];
        if (rol.idRol === this.rolSeleccionado.idRol) {
          cont++;
          break;
        }
      }
      if (cont > 0) {
        this.mensaje = `El rol se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        this.rolesSeleccionados.push(this.rolSeleccionado);
      }
    } else {
      this.mensaje = `Debe agregar un rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  limpiarControles() {
    this.rolesSeleccionados = [];
    this.rolSeleccionado = null;
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }
  estadoBotonRegistrar() {
    return ( this.idMenuSeleccionado === 0 || this.rolesSeleccionados.length === 0);
  }

  aceptar(estado) {
    if(estado==true){
    this.menu = new Menu();
    
    this.menu.idMenu = this.form.value['id'];//this.menuSeleccionado;
    this.menu.icono = this.form.value['icono'];
    this.menu.nombre = this.form.value['nombre'];
    this.menu.url = this.form.value['url'];

    this.menu.roles = this.rolesSeleccionados;

    console.log(this.menu);
    if (this.edicion) {
          //update
          this.menuService.modificar(this.menu).subscribe(data => {
            this.menuService.listarMenus().subscribe(menus => {
              this.menuService.menuCambio.next(menus);
              this.menuService.mensaje.next('Se Agrego Roles a Menú');
            });
          });
        }
      
         this.router.navigate(['roles-menu'])

  setTimeout(() => {    this.limpiarControles();    }, 2000);
  }
  }

}

