import { Component, OnInit, ViewChild } from '@angular/core';
import { Menu } from '../../_model/menu';
import { MenuService } from '../../_service/menu.service';

import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-roles-menu',
  templateUrl: './roles-menu.component.html',
  styleUrls: ['./roles-menu.component.css']
})
export class RolesMenuComponent implements OnInit {

  lista: Menu[] = [];
  displayedColumns = ['idMenu', 'nombre','roles', 'acciones'];
  dataSource: MatTableDataSource<Menu>;
  cantidad : number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor( public snackBar: MatSnackBar, private menuService: MenuService) { }
  ngOnInit() {
    this.menuService.menuCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.menuService.mensaje.subscribe(data => {        
        this.snackBar.open(data, 'Aviso', { duration: 2000 });
      });      
    });


    this.menuService.listarMenusPageable(0, 10).subscribe(data => {
     
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;
      console.log(menus);
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }



  mostrarMas(e: any){
    console.log(e);
    this.menuService.listarMenusPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(menus);
      
      this.dataSource.sort = this.sort;
      
    });
  }

}

