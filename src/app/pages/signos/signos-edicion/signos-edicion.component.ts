import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { SignosService } from './../../../_service/signos.service';
import { PacienteService } from './../../../_service/paciente.service';
import { Signos } from './../../../_model/signos';
import { Paciente } from '../../../_model/paciente';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  id: number;
  signos: Signos;
  pacientes: Paciente[] = [];
  form: FormGroup;
  edicion: boolean = false;

  myControlPaciente: FormControl = new FormControl();

maxFecha : Date = new Date();
fecha: Date = new Date();
pacienteSeleccionado: Paciente;

filteredOptions: Observable<any[]>;



  constructor(private signosService: SignosService,private pacienteService: PacienteService, private route: ActivatedRoute, private router: Router) { 
    this.signos = new Signos();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl((new Date()).toISOString()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
    });
    

  }

  ngOnInit() {
    this.listarPacientes();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
 

    this.filteredOptions = this.myControlPaciente.valueChanges
      .pipe(
        startWith(null),
        map(val => this.filter(val))
      );


  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e) {
    this.pacienteSeleccionado = e.option.value;
  }

  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.pacientes = data;
    });
  }

  private initForm() {
    if (this.edicion) {
      this.signosService.listarSignosPorId(this.id).subscribe(data => {
        let id = data.idSignos;
        let paciente=data.paciente;
        let fecha =new Date(data.fecha);
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmo = data.ritmo;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente':new FormControl(paciente),
          'fecha': new FormControl(fecha),
          'temperatura': new FormControl(temperatura),
          'pulso': new FormControl(pulso),
          'ritmo': new FormControl(ritmo)
        });
      });
    }
  }

  operar() {
    this.signos.idSignos = this.form.value['id'];
    this.signos.fecha = this.form.value['fecha'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmo = this.form.value['ritmo'];
    this.signos.paciente = this.form.value['paciente'];//this.pacienteSeleccionado;

    if (this.signos != null && this.signos.idSignos > 0) {
      this.signosService.modificar(this.signos).subscribe(data => {
        this.signosService.listarSignos().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensaje.next("Se modificó");
        });
      });
    } else {
      this.signosService.registrar(this.signos).subscribe(data => {
        this.signosService.listarSignos().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensaje.next("Se registró");
        });
      });
    }

    this.router.navigate(['signos']);
  }


}
