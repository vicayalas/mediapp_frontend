import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

import { SignosService } from './../../_service/signos.service';
import { Signos } from './../../_model/signos';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  displayedColumns = ['id','paciente', 'temperatura', 'pulso', 'fecha', 'ritmo', 'acciones'];
  dataSource: MatTableDataSource<Signos>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;

  constructor( private signosService: SignosService, public route: ActivatedRoute, public snackBar: MatSnackBar) { }

  ngOnInit() {    
    this.signosService.signosCambio.subscribe(data => {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  });

  this.signosService.mensaje.subscribe(data => {
    this.snackBar.open(data, null, {
      duration: 2000,
    });
  });

  this.signosService.listarSignos().subscribe(data => {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  eliminar(signos: Signos): void {
    this.signosService.eliminar(signos.idSignos).subscribe(data => {
      if (data === 1) {
        this.signosService.listarSignos().subscribe(data => {
          this.signosService.signosCambio.next(data);
          this.signosService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.signosService.mensaje.next("No se pudo eliminar");
      }
    });
  }

}
